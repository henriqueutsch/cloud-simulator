https://spin.atomicobject.com/2020/02/03/localstack-terraform-circleci/
https://github.com/localstack/localstack

ACM
API Gateway
CloudFormation
CloudWatch
CloudWatch Logs
DynamoDB
DynamoDB Streams
EC2
Elasticsearch Service
EventBridge (CloudWatch Events)
Firehose
IAM
Kinesis
KMS
Lambda
Redshift
Route53
S3
SecretsManager
SES
SNS
SQS
SSM
StepFunctions
STS


PRO
ECS/ECR/EKS
https://aws.amazon.com/pt/blogs/compute/a-guide-to-locally-testing-containers-with-amazon-ecs-local-endpoints-and-docker-compose/

CloudFront

ELB/ELBv2

IAM Security Policy Enforcement

Lambda Layers & Container Images
https://www.serverless.com/plugins/serverless-offline

RDS / Aurora Serverless

Amplify
API Gateway V2 (WebSockets support)
Application AutoScaling
AppSync
Athena
Batch
CloudTrail
Cognito
ElastiCache
EMR
Glacier / S3 Select
IoT
Kinesis Data Analytics
Managed Streaming for Kafka (MSK)
MediaStore
Neptune Graph DB
QLDB
Timestream
Transfer
XRay
Advanced persistence support for most services
Interactive UIs to manage resources
